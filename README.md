# README

The `jrebel-blossom-plugin` is a plug-in for JRebel to re-initialize the `BlossomDispatcherServlet` and in turn pick up new templates added with Magnolia Blossom.

## Usage

To use the plug-in you need to compile the artifact with the following command:

```bash
$ mvn install
```

Afterwards add the following VM arguments to a JRebel enabled process: 

```
-Drebel.plugins=<path-to-plugin>/jr-blossom-plugin.jar -Drebel.jr-magnolia-blossom=true
```

## Acknowledgements

Based on the [Spring Loaded plug-in](http://tobias-mattsson-magnolia.blogspot.de/2015/05/develop-faster-with-blossom-plugin-for.html) by [Tobias Mattsson](http://tobias-mattsson-magnolia.blogspot.de/).