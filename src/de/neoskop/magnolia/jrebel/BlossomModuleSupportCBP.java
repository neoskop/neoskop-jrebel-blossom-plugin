package de.neoskop.magnolia.jrebel;

import org.zeroturnaround.bundled.javassist.*;
import org.zeroturnaround.javarebel.Logger;
import org.zeroturnaround.javarebel.LoggerFactory;
import org.zeroturnaround.javarebel.integration.support.JavassistClassBytecodeProcessor;

/**
 * @author Arne Diekmann
 * @since 10.07.15
 */
public class BlossomModuleSupportCBP extends JavassistClassBytecodeProcessor {
	@Override
	public void process(ClassPool cp, ClassLoader cl, CtClass ctClass) throws Exception {
		for (CtConstructor ctConstructor : ctClass.getConstructors()) {
			ctConstructor.insertAfter("de.neoskop.magnolia.jrebel.BlossomPlugin.registerDispatcherServlet($0);");
		}
	}
}
