package de.neoskop.magnolia.jrebel;

import info.magnolia.module.blossom.annotation.*;
import org.springframework.stereotype.Controller;
import org.zeroturnaround.javarebel.ClassEventListener;
import org.zeroturnaround.javarebel.Logger;
import org.zeroturnaround.javarebel.LoggerFactory;

/**
 * @author Arne Diekmann
 * @since 10.07.15
 */
public class BlossomReloadListener implements ClassEventListener {
	public static final Logger LOG = LoggerFactory.getInstance();

	/**
	 * If the loaded or reloaded class has a Blossom annotation, mark blossom for reloading.
	 */
	public void onClassEvent(int eventType, Class clazz) {
		try {
			if (clazz.isAnnotationPresent(Controller.class)
					|| clazz.isAnnotationPresent(Template.class)
					|| clazz.isAnnotationPresent(Area.class)
					|| clazz.isAnnotationPresent(DialogFactory.class)
					|| clazz.isAnnotationPresent(ComponentCategory.class)
					|| clazz.isAnnotationPresent(VirtualURIMapper.class)) {
				BlossomPlugin.setReloadBlossom(true);
			}
		} catch (Exception e) {
			LOG.error(e);
		}
	}

	public int priority() {
		return 0;
	}
}
