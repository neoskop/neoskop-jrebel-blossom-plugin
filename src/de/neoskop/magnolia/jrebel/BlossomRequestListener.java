package de.neoskop.magnolia.jrebel;

import info.magnolia.module.blossom.dispatcher.BlossomDispatcherInitializedEvent;
import info.magnolia.module.blossom.render.BlossomDispatcherServlet;
import org.springframework.context.ConfigurableApplicationContext;
import org.zeroturnaround.javarebel.RequestListener;

/**
 * @author Arne Diekmann
 * @since 14.07.15
 */
public class BlossomRequestListener implements RequestListener {
	public boolean rawRequest(Object context, Object request, Object response) {
		return false;
	}

	public void beforeRequest() {
		if (BlossomPlugin.isReloadBlossom()) {
			reloadBlossom();
		}
	}

	/**
	 * Refreshes the application context if Blossom classes changed. If the Blossom plug-in does not handle that event
	 * on its own, publish a {@link BlossomDispatcherInitializedEvent}.
	 */
	private void reloadBlossom() {
		for (BlossomDispatcherServlet dispatcherServlet : BlossomPlugin.getDispatcherServlets()) {
			ConfigurableApplicationContext applicationContext = (ConfigurableApplicationContext) dispatcherServlet.getWebApplicationContext();

			if (applicationContext != null) {
				applicationContext.refresh();

				if (BlossomPlugin.isSendBlossomDispatcherInitializedEvent()) {
					applicationContext.publishEvent(new BlossomDispatcherInitializedEvent(dispatcherServlet));
				}
			}
		}

		BlossomPlugin.setReloadBlossom(false);
	}

	public void requestFinally() {
	}

	public int priority() {
		return 0;
	}
}
