package de.neoskop.magnolia.jrebel;

import info.magnolia.module.blossom.render.BlossomDispatcherServlet;
import org.zeroturnaround.javarebel.*;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Arne Diekmann
 * @since 09.07.15
 */
public class BlossomPlugin implements Plugin {
	public static final Logger LOG = LoggerFactory.getInstance();
	private static Set<BlossomDispatcherServlet> dispatcherServlets = new HashSet<BlossomDispatcherServlet>();
	private static boolean sendBlossomDispatcherInitializedEvent;
	private static boolean reloadBlossom = false;

	public static boolean isSendBlossomDispatcherInitializedEvent() {
		return sendBlossomDispatcherInitializedEvent;
	}

	public static boolean isReloadBlossom() {
		return reloadBlossom;
	}

	public static void setReloadBlossom(boolean reloadBlossom) {
		BlossomPlugin.reloadBlossom = reloadBlossom;
	}

	public void preinit() {
		Integration i = IntegrationFactory.getInstance();
		ClassLoader cl = BlossomPlugin.class.getClassLoader();
		i.addIntegrationProcessor(cl, "info.magnolia.module.blossom.render.BlossomDispatcherServlet", new BlossomModuleSupportCBP());
		ReloaderFactory.getInstance().addClassReloadListener(new BlossomReloadListener());
		ReloaderFactory.getInstance().addClassLoadListener(new BlossomReloadListener());
		RequestIntegrationFactory.getInstance().addRequestListener(new BlossomRequestListener());
		checkForOnRefresh();
	}

	/**
	 * Checks whether {@link BlossomDispatcherServlet} has the "onRefresh" method, which it has since Blossom 3.0.7+
	 */
	private void checkForOnRefresh() {
		try {
			BlossomDispatcherServlet.class.getDeclaredMethod("onRefresh", new Class[]{Class.forName("org.springframework.context.ApplicationContext")});
			sendBlossomDispatcherInitializedEvent = false;
		} catch (NoSuchMethodException e) {
			sendBlossomDispatcherInitializedEvent = true;
		} catch (ClassNotFoundException e) {
			LOG.error(e);
		}
	}

	/**
	 * method is called whenever a {@link BlossomDispatcherServlet} is instantiated (See {@link BlossomModuleSupportCBP#process})
	 * @param dispatcherServlet instance of the new {@link BlossomDispatcherServlet}
	 */
	public static void registerDispatcherServlet(Object dispatcherServlet) {
		if (dispatcherServlet instanceof BlossomDispatcherServlet) {
			dispatcherServlets.add((BlossomDispatcherServlet) dispatcherServlet);
		}
	}

	public static Set<BlossomDispatcherServlet> getDispatcherServlets() {
		return dispatcherServlets;
	}

	public boolean checkDependencies(ClassLoader classLoader, ClassResourceSource classResourceSource) {
		return classResourceSource.getClassResource("info.magnolia.module.blossom.dispatcher.BlossomDispatcher") != null;
	}

	public String getId() {
		return "jr-magnolia-blossom";
	}

	public String getName() {
		return "Magnolia Blossom Plugin";
	}

	public String getDescription() {
		return "<li>Reload BlossomDispatcherServlet if a Blossom annotation was changed</li>";
	}

	public String getAuthor() {
		return "Arne Diekmann";
	}

	public String getWebsite() {
		return "http://www.neoskop.de";
	}

	public String getSupportedVersions() {
		return "6.2.1";
	}

	public String getTestedVersions() {
		return "6.2.1";
	}
}
